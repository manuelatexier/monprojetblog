import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mon-projet-blog';

  posts = [
    {title: "premier Post",
      content: "Blabla1"},
    {title: "deuxième Post",
      content: "Blabla2"},
    {title: "troisième Post",
      content: "Blabla3"},
  ];

}

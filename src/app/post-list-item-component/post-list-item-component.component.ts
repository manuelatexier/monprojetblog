import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() posttitle : string;
  @Input() postcontent: string;
  numberLove: number = 0;
  numberNotLove: number = 0;
  created_at = new Date();

  constructor() { }

  ngOnInit() {
  }

  addOneToLove(){
   this.numberLove = this.numberLove + 1;
   return this.numberLove
  }

  addOneToNotLove() {
    this.numberNotLove = this.numberNotLove + 1;
    return this.numberNotLove
  }

}
